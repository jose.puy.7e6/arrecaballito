﻿using System;

namespace ArreCaballito
{
    class ArreCaballito
    {
        static void Main()
        {
            Caballo[] caballos = new Caballo[] {
                new Caballo("Perdigon"),
                new Caballo("Roach"),
                new Caballo("Epona"),
                new Caballo("Fantasma"),
                new Caballo("Lechuguino"),
                new Caballo("Juan")};

            if (Console.ReadKey().Key == ConsoleKey.C)
            {
                Console.WriteLine(" apretada");

                foreach (Caballo c in caballos)
                {
                    c.Hilo.Start();
                }
            }
        }
    }
}
