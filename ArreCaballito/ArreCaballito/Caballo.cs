﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ArreCaballito
{
    public class Caballo
    {
        public string Nombre { get; set; }

        public int posIn { get; set; }

        public int velocidad { get; set; }

        public Thread Hilo { get; set; }

        readonly Random rnd = new Random();

        public Caballo(string nombre)
        {
            this.Nombre = nombre;

            this.posIn = 0;

            this.velocidad = rnd.Next(1, 7);

            this.Hilo = new Thread(() =>
            {
                while (posIn < 50)
                {
                    Console.WriteLine(this.Nombre+ "\n");
                    Move();
                    for (int i = 0; i < posIn; i++)
                    {
                        Console.Write("-");
                    }
                    Thread.Sleep(1000);
                    //Console.Clear();
                }
            });
        }

        public void Move()
        {
            posIn += velocidad;
        }
    }
}
